local addon, dark_addon = ...

dark_addon.rotation.timer = {
  lag = 0
}

local last_duration = false

function dark_addon.rotation.tick(ticker)
  local toggled = dark_addon.settings.fetch_toggle('master_toggle', false)
  local turbo = dark_addon.settings.fetch('_engine_turbo', false)
  local castclip = dark_addon.settings.fetch('_engine_castclip', 0.15)
  local gcd_wait = 0
  ticker._duration = dark_addon.settings.fetch('_engine_tickrate', 0.1)

  if ticker._duration ~= last_duration then
    last_duration = ticker._duration
    dark_addon.console.debug(2, 'engine', 'engine', string.format('Ticket Rate: %sms', last_duration * 1000))
  end

  if not toggled then
    dark_addon.interface.status('Ready...')
    return
  end

  local gcd_start, gcd_duration = GetSpellCooldown(61304)
  local do_gcd = dark_addon.settings.fetch('_engine_gcd', true)
  if gcd_start > 0 then
    dark_addon.tmp.store('lastgcd', gcd_duration)
    gcd_wait = (gcd_duration - (GetTime() - gcd_start)) or 0
  end

  if dark_addon.rotation.active_rotation then
    if IsMounted() then return end

    if not turbo and do_gcd and gcd_wait > 0 then
      if dark_addon.rotation.active_rotation.gcd then
        return dark_addon.rotation.active_rotation.gcd()
      else
        return
      end
    end

    if UnitAffectingCombat('player') then
      dark_addon.rotation.active_rotation.combat()
    else
      dark_addon.rotation.active_rotation.resting()
      dark_addon.interface.status('Resting...')
    end
  end
end

dark_addon.on_ready(function()
  dark_addon.rotation.timer.ticker = C_Timer.NewAdvancedTicker(0.1, dark_addon.rotation.tick)
end)
